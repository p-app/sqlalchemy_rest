# SQLAlchemy REST

Various abstract classes and mixins for REST-like 
implementation with sqlalchemy orm-based queries.

[Pyramid](https://trypyramid.com/) and [aiohttp](https://docs.aiohttp.org) 
framework mixins are implemented out of the box.

Another frameworks can use implemented abstract classes.
